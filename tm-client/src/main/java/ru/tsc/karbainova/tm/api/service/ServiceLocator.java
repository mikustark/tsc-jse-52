package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import ru.tsc.karbainova.tm.endpoint.*;

public interface ServiceLocator {

    IPropertyService getPropertyService();

    ICommandService getCommandService();

    //add endpoints

    @NonNull TaskEndpoint getTaskEndpoint();

    @NonNull ProjectEndpoint getProjectEndpoint();

    @NonNull UserEndpoint getUserEndpoint();

    @NonNull AdminUserEndpoint getAdminUserEndpoint();

    SessionEndpoint getSessionEndpoint();

    //
//    //Завести сервис и перенести туда get и set для сессии
//    // и с бутстрапа убрать их реализацию не забыть
//
    SessionDTO getSession();

    void setSession(SessionDTO session);
}
