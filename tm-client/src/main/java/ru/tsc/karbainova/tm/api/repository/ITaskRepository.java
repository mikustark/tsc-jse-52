package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.endpoint.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<TaskDTO> {
    void add(String userId, TaskDTO task);

    void remove(String userId, TaskDTO task);

    boolean existsById(String userId, String id);

    void clear();

    List<TaskDTO> findAll();

    List<TaskDTO> findAll(String userId);

    List<TaskDTO> findAll(String userId, Comparator<TaskDTO> comparator);

    void clear(String userId);

    TaskDTO findById(String userId, String id);

    TaskDTO findByIndex(String userId, int index);

    TaskDTO findByName(String userId, String name);

    TaskDTO removeById(String userId, String id);

    TaskDTO removeByName(String userId, String name);

    TaskDTO removeByIndex(String userId, int index);

    TaskDTO taskUnbindById(String userId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    TaskDTO bindTaskToProjectById(String userId, String projectId, String taskId);

    List<TaskDTO> findAllTaskByProjectId(String userId, String projectId);

}
