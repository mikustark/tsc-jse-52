package ru.tsc.karbainova.tm.api;

import lombok.NonNull;

public interface ISignatureSetting {
    @NonNull
    Integer getSignatureIteration();

    @NonNull
    String getSignatureSecret();
}
