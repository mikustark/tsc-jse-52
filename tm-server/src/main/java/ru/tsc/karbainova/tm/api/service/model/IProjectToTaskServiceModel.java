package ru.tsc.karbainova.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface IProjectToTaskServiceModel {
    @SneakyThrows
    void bindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @SneakyThrows
    void unbindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    @NotNull
    @SneakyThrows
    List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    void removeProjectById(@Nullable String id);

    @SneakyThrows
    void removeProjectByName(@Nullable String userId, @Nullable String name);

    @SneakyThrows
    void removeProjectByIndex(@Nullable String userId, @NotNull Integer index);
}
