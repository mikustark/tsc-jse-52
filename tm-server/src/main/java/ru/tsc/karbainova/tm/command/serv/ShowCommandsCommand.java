package ru.tsc.karbainova.tm.command.serv;

import ru.tsc.karbainova.tm.command.AbstractCommand;

public class ShowCommandsCommand extends AbstractCommand {
    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "All commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands().values()) {
            System.out.println(command.toString());
        }
    }
}
